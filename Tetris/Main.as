﻿package {
	import flash.display.Sprite;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	import flash.events.KeyboardEvent;
	public class Main extends Sprite {
		private const TS:uint=24; //size of each tile in pixel
		private var fieldArray:Array; //array that will numerically represent the game field
		private var fieldSprite:Sprite; //DisplayObject that will graphically render the game field
		private var tetrominoes:Array = new Array(); //array is the four-dimensional array containing all tetrominoes information
		private var colors:Array=new Array();//colors of each tetrominoes
		private var tetromino:Sprite; //DisplayObject representing the tetromino itself
		private var currentTetromino:uint; //number of the tetromino currently in game, and will range from 0 to 6
		private var nextTetromino:uint; // next tetromino whcih will be displayed on the side display
		private var currentRotation:uint; // rotation of the tetromino and will range from 0 to 3 since a tetromino can have four distinct rotations
		private var tRow:int; //represent the current horizontal position of the tetromino in the game field
		private var tCol:int; // represent the current vertical position of the tetromino in the game field
		private var timeCount:Timer=new Timer(500); // timer for falling pieces
		private var gameOver:Boolean=false; // to check game over
		
		public function Main() {
			//draw the background
			generateField();
			//initialize tetrominoes-related arrays
			initTetrominoes();
			//generate the next piece which will be shown in the right side display
			nextTetromino=Math.floor(Math.random()*7);
			//generate the tetrominoes
			generateTetromino();
			//listen to keyboard press
			stage.addEventListener(KeyboardEvent.KEY_DOWN,onKDown);
			// tetris!!  
		}
	//Draw a set of squares to represent the game field.
	private function generateField():void {
	//the two alternating colors to be used for out game field
	var colors:Array=new Array("0x444444","0x555555");
	fieldArray=new Array  ;
	//the sprite that will hold the game field
	var fieldSprite:Sprite=new Sprite  ;
	addChild(fieldSprite);
	//drawing the field
	fieldSprite.graphics.lineStyle(0,0x000000);
	for (var i:uint=0; i<20; i++) {
		fieldArray[i]=new Array  ;
		for (var j:uint=0; j<10; j++) {
			fieldArray[i][j]=0;
			fieldSprite.graphics.beginFill(colors[(j%2+i%2)%2]);
			fieldSprite.graphics.drawRect(TS*j,TS*i,TS,TS);
			fieldSprite.graphics.endFill();
		}
	}
	}
	//end of drawing the field
	/*the names and colors of the each piece
	I – color: cyan (0x00FFFF)
	T – color: purple (0xAA00FF)
	L – color: orange (0xFFA500)
	J – color: blue (0x0000FF)
	Z – color: red (0xFF0000)
	S – color: green (0x00FF00)
	O – color: yellow (0xFFFF00)
	*/
	// the init codes for individual tetrominoes

	private function initTetrominoes():void {
	// I
	tetrominoes[0]=[[[0,0,0,0],[1,1,1,1],[0,0,0,0],[0,0,0,0]],
					[[0,1,0,0],[0,1,0,0],[0,1,0,0],[0,1,0,0]]];
	colors[0]=0x00FFFF;
	// T
	tetrominoes[1]=[[[0,0,0,0],[1,1,1,0],[0,1,0,0],[0,0,0,0]],
					[[0,1,0,0],[1,1,0,0],[0,1,0,0],[0,0,0,0]],
					[[0,1,0,0],[1,1,1,0],[0,0,0,0],[0,0,0,0]],
					[[0,1,0,0],[0,1,1,0],[0,1,0,0],[0,0,0,0]]];
	colors[1]=0x767676;
	// L
	tetrominoes[2]=[[[0,0,0,0],[1,1,1,0],[1,0,0,0],[0,0,0,0]],
					[[1,1,0,0],[0,1,0,0],[0,1,0,0],[0,0,0,0]],
					[[0,0,1,0],[1,1,1,0],[0,0,0,0],[0,0,0,0]],
					[[0,1,0,0],[0,1,0,0],[0,1,1,0],[0,0,0,0]]];
	colors[2]=0xFFA500;
	// J
	tetrominoes[3]=[[[1,0,0,0],[1,1,1,0],[0,0,0,0],[0,0,0,0]],
					[[0,1,1,0],[0,1,0,0],[0,1,0,0],[0,0,0,0]],
					[[0,0,0,0],[1,1,1,0],[0,0,1,0],[0,0,0,0]],
					[[0,1,0,0],[0,1,0,0],[1,1,0,0],[0,0,0,0]]];
	colors[3]=0x0000FF;
	// Z
	tetrominoes[4]=[[[0,0,0,0],[1,1,0,0],[0,1,1,0],[0,0,0,0]],
					[[0,0,1,0],[0,1,1,0],[0,1,0,0],[0,0,0,0]]];
	colors[4]=0xFF0000;
	// S
	tetrominoes[5]=[[[0,0,0,0],[0,1,1,0],[1,1,0,0],[0,0,0,0]],
					[[0,1,0,0],[0,1,1,0],[0,0,1,0],[0,0,0,0]]];
	colors[5]=0x00FF00;
	// O
	tetrominoes[6]=[[[0,1,1,0],[0,1,1,0],[0,0,0,0],[0,0,0,0]]];
	colors[6]=0xFFFF00;
	}
	//end of init tetrominoes
	
	//adding individual piece on the game fied
	private function generateTetromino():void {
		if (! gameOver) {
		currentTetromino=nextTetromino; //asign the next piece to be the current
		nextTetromino=Math.floor(Math.random()*7); //generates a random integer number between 0 and 6 (the possible tetrominoes)
		drawNext(); //generate next piece
		currentRotation=0; // can be change but 0 is usually the rotation of the pieces in tetris games
		tRow=0; // place the tetromino at the very top of the game field
		if (tetrominoes[currentTetromino][0][0].indexOf(1)==-1) //making sure that tetromino will always be to topmost part of the field
		{ 
			tRow=-1;
		}
		tCol=3; // bec game field has 10 column. to center it we use 3: (10-4)/2 = 3.
		drawTetromino();
	
		
		if (canFit(tRow,tCol,currentRotation)) {
			//starting the timer for auto descend
			timeCount.addEventListener(TimerEvent.TIMER,onTime);
			timeCount.start();
		} else {
			gameOver=true;
		}
		//end of checking game over
		}
	}
	//end of adding the piece on the game field
	
	//drawing individual tetrominoes
	private function drawTetromino():void {
	var ct:uint=currentTetromino;
	tetromino=new Sprite; //create the contaoner for the tetromino
	addChild(tetromino); // add it to the field
	tetromino.graphics.lineStyle(0,0x000000);
	for (var i:int=0; i<tetrominoes[ct][currentRotation].length; i++) {
		for (var j:int=0; j<tetrominoes[ct][currentRotation][i].length; j++) {
			if (tetrominoes[ct][currentRotation][i][j]==1) {
				tetromino.graphics.beginFill(colors[ct]);
				tetromino.graphics.drawRect(TS*j,TS*i,TS,TS);
				tetromino.graphics.endFill();
			}
		}
	}
	placeTetromino();
	}
	//end drawing individual tetrominoes
	
	//set the position of the teromino after cretion
	private function placeTetromino():void {
	tetromino.x=tCol*TS;
	tetromino.y=tRow*TS;
	}
	//end of setting the position of the teromino after cretion
	
	//moving the pieces
	private function onKDown(e:KeyboardEvent):void {
	if (! gameOver) {
	switch (e.keyCode) {
		case 37 :
			if (canFit(tRow,tCol-1,currentRotation)) {
				tCol--;
				placeTetromino();
			}
			break;
		case 38 :
			var ct:uint=currentRotation;
			trace("up");
			var rot:uint=(ct+1)%(tetrominoes[currentTetromino].length);
			if (rot==4) { //re iterate the array when rotation reaches end
			rot=1;
			}
			if (canFit(tRow,tCol,rot)) {
				currentRotation=rot;
				removeChild(tetromino);
				drawTetromino();
				placeTetromino();
			}
			break;
		case 39 :
			if (canFit(tRow,tCol+1,currentRotation)) {
				tCol++;
				placeTetromino();
			}
			break;
		case 40 :
			if (canFit(tRow+1,tCol,currentRotation)) {
				tRow++;
				placeTetromino();
			} else {
				landTetromino();
				generateTetromino();
			}
			break;
		}
		//end for if game over
		}
	}
	//end of moving the pieces
	
	//checking if the piece can be moveable to the let or right
	private function canFit(row:int,col:int, side:uint):Boolean {
	var ct:uint=currentTetromino;
	for (var i:int=0; i<tetrominoes[ct][side].length; i++) {
		for (var j:int=0; j<tetrominoes[ct][side][i].length; j++) {
			if (tetrominoes[ct][side][i][j]==1) {
				// out of left boundary
				if (col+j<0) {
					return false;
				}
				// out of right boundary
				if (col+j>9) {
					return false;
				}
				// out of bottom boundary
				if (row+i>19) {
					return false;
				}
				// over another tetromino
				if (fieldArray[row+i][col+j]==1) {
					return false;
				}
			}
		}
	}
	return true;
	}
	//end of checking if the piece can be moveable to the let or right
	
	//managing landing of tetromino
	private function landTetromino():void {
	var ct:uint=currentTetromino;
	var landed:Sprite;
	for (var i:int=0; i<tetrominoes[ct][currentRotation].length; i++) {
		for (var j:int=0; j<tetrominoes[ct][currentRotation][i].length; j++) {
			if (tetrominoes[ct][currentRotation][i][j]==1) {
				landed=new Sprite  ;
				addChild(landed);
				landed.graphics.lineStyle(0,0x000000);
				landed.graphics.beginFill(colors[currentTetromino]);
				landed.graphics.drawRect(TS*(tCol+j),TS*(tRow+i),TS,TS);
				landed.graphics.endFill();
				landed.name="r"+(tRow+i)+"c"+(tCol+j);
				fieldArray[tRow+i][tCol+j]=1;
			}
		}
	}
	removeChild(tetromino); // the old tetromino is removed
	timeCount.removeEventListener(TimerEvent.TIMER,onTime); //stop the previous timer
	timeCount.stop();
	checkForLines();
	}
	//end of managing landing of tetromino
	
	//checking for completed lines
	private function checkForLines():void {
	for (var i:int=0; i<20; i++) {
		if (fieldArray[i].indexOf(0)==-1) {
			for (var j:int=0; j<10; j++) {
				fieldArray[i][j]=0;
				removeChild(getChildByName("r"+i+"c"+j));
			}
			for (j=i; j>=0; j--) {
				for (var k:int=0; k<10; k++) {
					if (fieldArray[j][k]==1) {
						fieldArray[j][k]=0;
						fieldArray[j+1][k]=1;
						getChildByName("r"+j+"c"+k).y+=TS;
						getChildByName("r"+j+"c"+k).name="r"+(j+1)+"c"+k;
					}
				}
			}
		}
	}
	}
	//end of checking for completed lines
	
	// the timer listener
	private function onTime(e:TimerEvent):void {
	if (canFit(tRow+1,tCol,currentRotation)) {
		tRow++;
		placeTetromino();
	} else {
		landTetromino();
		generateTetromino();
	}
	}
	//end of timer listener
	
	//generate next piece and show it on the right side
	private function drawNext():void {
	if (getChildByName("next")!=null) {
		removeChild(getChildByName("next"));
	}
	var next_t:Sprite=new Sprite  ;
	next_t.x=300;
	next_t.name="next";
	addChild(next_t);
	next_t.graphics.lineStyle(0,0x000000);
	for (var i:int=0; i<tetrominoes[nextTetromino][0].length; i++) {
		for (var j:int=0; j<tetrominoes[nextTetromino][0][i].length; j++) {
			if (tetrominoes[nextTetromino][0][i][j]==1) {
				next_t.graphics.beginFill(colors[nextTetromino]);
				next_t.graphics.drawRect(TS*j,TS*i,TS,TS);
				next_t.graphics.endFill();
			}
		}
	}
	}
	//end of generating next piece and show it on the right side
	}
	
	
}
